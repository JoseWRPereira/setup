#!/bin/bash
######################################## Terminator
echo "Instalar o Terminator? [s/n]"
read terminator

if test "$terminator" = s
then
	echo "Instalando o terminator..."
	sudo apt install terminator
else
	echo "Não instalar o terminator"
fi

####################################### 
dir="~/app/SimpleNote/"
if [ ! -d "$dir" ] 
then 
	echo "O diretório $dir não existe"
	mkdir $(dir)
else
	echo "O diretório $dir existe"
fi

cd $(dir)
wget https://github.com/Automattic/simplenote-electron/releases/download/v2.4.0/Simplenote-linux-2.4.0-amd64.deb -v --show-progress
echo "Instalando o Simplenote... "
sudo dpkg -i Simplenote-linux-2.4.0-amd64.deb
echo "Fim da instalação"

# Atualização do sistema
#sudo apt update
#sudo apt upgrade

# Programa makefile
#sudo apt intall make


