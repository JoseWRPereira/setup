# Usage:
# make 				# Execute all instalations
# make <target>			# Execute only taget installation

#
# Vars
#
urlMPLABX = https://www.microchip.com/content/dam/mchp/documents/DEV/ProductDocuments/SoftwareTools/mplabx-v5.45-linux-installer.tar
urlXC8 = https://www.microchip.com/mplabxc8linux 
MPLABX_XC8_dir = ~/MPLABXProjects/installer

.PHONY = all atualizar

all: atualizar xclip editor

# Atualização do sistema
atualizar: 
	sudo apt update
	sudo apt upgrade

# Aplicativo para copiar texto em área de transferência
xclip: 
	sudo apt install xclip

# Editor de código fonte
editor:
	sudo apt install vim 

# i3 window manager
i3:
	sudo apt install i3

# MPLAB-X IDE e XC8
mplabx:
xc8:

	wget $(urlMPLABX) -v --show-progress
	wget $(urlXC8) -v --show-progress
	chmod +x mplabxc8linux
	mv mplabxc8linux $(MPLABX_XC8_dir)
#	sudo apt install libc6:i386
#	sudo apt install lib32stdc++-10-dev
#	sudo apt install libexpat1:i386
#	sudo apt install libx11-dev:i386
#	sudo apt install libxext-dev:i386


texstudio:
	sudo apt install texstudio 
	sudo apt install texstudio-doc 
	sudo apt install texlive-pictures 
	sudo apt install texlive-science 
	sudo apt install texlive-latex-extra 
	sudo apt install texlive-lang-portuguese
	sudo apt install texlive-publishers
	sudo apt install texlive-full


teams:
	sudo dpkg -i ~/app/Teams/teams_1.3.00.30857_amd64.deb


snap:
	sudo apt install snapd

chromium: snap
	sudo snap install chromium
	snap run chromium


epiphany:
	sudo apt install epiphany-browser


jekyll:
	sudo apt install jekyll
	sudo apt install ruby
	sudo apt install ruby2.7-dev
	sudo apt install curl
	sudo apt install nodejs build-essential
	sudo gem install jekyll --user-install

vscode:
	sudo dpkg -i ~/app/vscode/code*.deb


esptool:
	sudo apt install esptool


peek:
	sudo apt install peek

